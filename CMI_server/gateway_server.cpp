/*CanMoveIt Remote client 1.4.1
Copyright (c) 2014, Icarus Technology SL, All rights reserved.

Contact: Davide Faconti  faconti@icarustechnology.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <stdio.h>

int main(int argc, char** argv)
{
    printf("\nStarting CMI Master server version 1.4.1\n\n");

    return 1;
}

/*#include "OS/PeriodicTask.h"
#include "cmi/CO301_interface.h"
#include "cmi/MAL_CANOpen402.h"
#include "cmi/CMI.h"
#include <zmq.h>
#include "msg_zmq_cmi.pb.h"
#include <boost/property_tree/xml_parser.hpp>

using namespace CanMoveIt;
void* context;
std::map<unsigned, float> reference_velocity ;
std::map<unsigned, int>   zero_velocity_counter ;

#define  XML_CONFIG "/etc/cmi/gateway_server.xml"

int velocity_watchdog[128];


#define QUOTE(name) #name
#define STR(macro) QUOTE(macro)

#ifdef CMI_LIB_SUFFIX
    #define CMI_LIB_SUFFIX_STR STR(CMI_LIB_SUFFIX)
#else
    #define CMI_LIB_SUFFIX_STR
#endif

char  can_driver_filename[]  =   "/usr/local/lib/libdriver_socket" CMI_LIB_SUFFIX_STR ".so" ;


void command_server(void*)
{

    const MotorList& motor_list = cmi_getMotorList();

    std::cout << COLOR_YELLOW;
    printf(" -------- starting command server  ----------\n");
    std::cout << COLOR_RESET ;

    // connect ZMQ

    void* server_port = zmq_socket(context, ZMQ_REP);
    if( zmq_bind( server_port, "tcp://*:5999") != 0)
    {
        printf("Can't start ZMQ server: %s\n", zmq_strerror (errno) );
        throw SystemException(" Can't start ZMQ server");
    }

    CMI::MsgCommand cmd;

    while(1)
    {
        std::string reply("OK");
        zmq_msg_t msg;
        int rc = zmq_msg_init (&msg);
        assert (rc == 0);
        //  printf("server: waiting for command\n");
        zmq_recvmsg( server_port, &msg, 0 );
        // printf("server: command received\n");

        cmd.ParseFromArray(zmq_msg_data(&msg), zmq_msg_size(&msg) );
        zmq_msg_close (&msg);

        for (int i=0; i< cmd.axis_cmd_size(); i++)
        {
            // printf("server: processign command %d\n", i);
            const ::CMI::MsgCommand_AxisCommand& acmd = cmd.axis_cmd(i);
            UInt16 motor_id = acmd.motor_id();

            MotorList::const_iterator it = motor_list.find( motor_id ) ;
            if( it == motor_list.end())
            {
                reply = std::string("NOT_OK");
            }
            else{
                MAL_InterfacePtr motor_ptr = it->second;
                switch( acmd.type() )
                {
                case CMI::MsgCommand_AxisCommandType_SET_VELOCITY :
                {
                    motor_ptr->setModeOperation( PROFILED_VELOCITY_MODE);
                    motor_ptr->setProfiledVelocityTarget(  acmd.argument_1() );
                    reference_velocity[ motor_id ] = acmd.argument_1();

                    velocity_watchdog[ motor_id ] = 0;
                }break;

                case CMI::MsgCommand_AxisCommandType_SET_POSITION :
                {
                    motor_ptr->setModeOperation( PROFILED_POSITION_MODE);
                    unsigned flags = static_cast<unsigned>( acmd.argument_2() );
                    motor_ptr->setProfiledPositionTarget( acmd.argument_1(), flags );
                }break;

                case CMI::MsgCommand_AxisCommandType_SET_PROFILE_PARAMETER :
                {
                    ProfileParameter param = static_cast<ProfileParameter>(acmd.argument_1());
                    motor_ptr->setProfileParameter( param, acmd.argument_2() );
                }break;

                case CMI::MsgCommand_AxisCommandType_SET_CURRENT_LIMIT :
                {
                    UInt16 max_curr_mAmp = 1000 * acmd.argument_1();
                    motor_ptr->setCurrentLimit( max_curr_mAmp );
                }break;

                case CMI::MsgCommand_AxisCommandType_RESET_AXIS :
                {
                    if( motor_ptr->getStatus() != OPERATION_ENABLED)
                    {
                        motor_ptr->resetFaults();
                        motor_ptr->startDrive();
                        reference_velocity[ motor_id ] = 0;
                    }
                }break;

                case CMI::MsgCommand_AxisCommandType_EMERGENCY_STOP :
                {
                    motor_ptr->stopDrive();
                    reference_velocity[ motor_id ] = 0;
                }break;

                default: reply = std::string("NOT_OK");
                }
            }
        }// end for

        zmq_send( server_port, reply.c_str(), reply.size(), 0);

    }// end of while
    printf(" -------- CLOSING command server\n ----------\n");
}

//--------------------------------------------------------
class StatusUpdater: public PeriodicTask
{
    int _TCP_velocity_timeout;
    void* publisher_port;
    long unsigned cycle;
public:
    const MotorList& _motor;

    StatusUpdater ( ): _motor(  cmi_getMotorList() ) {}
    void startHook();
    void updateHook();
    void stopHook()  {}
    void timeout(int device_id);

    void targetReached(UInt16 device_id, EventData const& event);
    void faultDetected(UInt16 device_id, EventData const& event);

private:

};

int main(int argc, char** argv)
{
    printf("\nStarting CMI Master server version 1.4.1\n\n");

    return 1;
}
  /*  system("ifconfig can0 down;ifconfig can0 up; ifconfig can1 down;ifconfig can1 up");

    for (int i=0; i<128; i++ ) velocity_watchdog[i] = -1;


    context = zmq_ctx_new();
    try{
        LoadCanDriver( "/usr/local/lib/libdriver_socket.so" );

        // the xml file contain all the setup we need
        cmi_init( XML_CONFIG );


        BOOST_FOREACH(const MotorList::value_type& it, cmi_getMotorList()  )
        {
            cmi_configureMotor(it.second);
        }

        Thread server_thread;
        server_thread.start( command_server , NULL);

        StatusUpdater task;
        task.start_execution( Milliseconds(10) );

        task.wait_completion();
    }
    catch( Exception& exc)
    {
        std::cout<< exc.what() << std::endl;
        std::cout<< "\n\nPress Enter to continue" <<std::endl;
        getchar();
        return 1;
    }
    return 0;
}


void StatusUpdater::startHook()
{
    // read some extra information from file
    using boost::property_tree::ptree;
    ptree pt;
    read_xml( XML_CONFIG, pt );

    _TCP_velocity_timeout = 1000;
    int CAN_heartbeat_timeout = -1;

    BOOST_FOREACH( ptree::value_type & v, pt.get_child( "GatewayParameters" ) )
    {
        std::string param_name  =  v.second.get<std::string>( "<xmlattr>.name" );
        std::string param_value =  v.second.get<std::string>( "<xmlattr>.value" );
        if( param_name.compare( "update_frequency_ms" ) == 0)
        {
            long period = atoi( param_value.c_str() );
            if( period <2 ) throw SystemException("wrong update_frequency_ms");
            this->setPeriod( Milliseconds(period) );
        }
        else if( param_name.compare( "TCP_velocity_timeout_ms" ) == 0)
        {
            _TCP_velocity_timeout = atoi( param_value.c_str() );
            if( _TCP_velocity_timeout <0 ) throw SystemException("wrong TCP_velocity_timeout_ms");

        }
        else if( param_name.compare( "CAN_heartbeat_timeout_ms" ) == 0)
        {
            CAN_heartbeat_timeout = atoi( param_value.c_str() );
            if( CAN_heartbeat_timeout <0 ) throw SystemException("wrong CAN_heartbeat_timeout_ms");
            if( Milliseconds( CAN_heartbeat_timeout/2) < this->getPeriod() )
            {
                printf("CAN_heartbeat_timeout is too small");
                throw SystemException("wrong CAN_heartbeat_timeout_ms");
            }
        }
        else {
            std::cout << param_name << " not supported.\n";
        }
    }


    publisher_port = zmq_socket(context, ZMQ_PUB);
    if( zmq_bind( publisher_port, "tcp://*:5998") != 0)
    {
        printf("can't bind ZMQ_PUB on port 5998\n");
        throw SystemException( "can't bind ZMQ_PUB");
    }

    BOOST_FOREACH(const MotorList::value_type& it, cmi_getMotorList()  )
    {
        CO301_InterfacePtr co301 = cmi_getCO301_Interface( it.first );

        co301->sdoWrite( co301->findObjectKey(0x2FFD,0), 2); // maximum current range. Venus specific
        zero_velocity_counter[ it.first ] = 0;

        // don't activate the heartbeat
        co301->sdoWrite( co301->findObjectKey(0x1017,0), (UInt16) 0);

        co301->sdoWrite( co301->findObjectKey(0x100C,0), (UInt16) CAN_heartbeat_timeout);
        co301->sdoWrite( co301->findObjectKey(0x100D,0), (UInt8) 1);

        co301->sendNMT_lifeguard();

        co301->events()->add_subscription(EVENT_PROFILED_POSITION_REACHED,  CALLBACK_SYNCH,
                                          boost::bind( &StatusUpdater::targetReached, this, _1, _2)  );

        co301->events()->add_subscription(EVENT_EMERGENCY_FAULT,  CALLBACK_SYNCH,
                                          boost::bind( &StatusUpdater::faultDetected, this, _1, _2)  );

    }
}

void StatusUpdater::updateHook()
{
    cycle = 0;

    CMI::MsgStatus status;
    int i=0;

    status.set_timestamp ( time_since_epoch<Milliseconds>() );
    status.set_master_status( 0 ); //TODO

    BOOST_FOREACH(const MotorList::value_type& it, cmi_getMotorList()  )
    {
        CMI::MsgStatus_AxisInfo* info = status.add_info();
        double vel= -1,pos = -1;
        Int32 curr = -1;

        MAL_InterfacePtr motor_ptr = it.second;
        int motor_id = it.first;

        // TODO: PDO mapping fails if there is a reset of CAN bus
        CO301_InterfacePtr co301 = cmi_getCO301_Interface( motor_id );
        co301->sdoObjectRequest( co301->findObjectKey(POSITION_ACTUAL_VALUE) );
        co301->sdoObjectRequest( co301->findObjectKey(VELOCITY_ACTUAL_VALUE) );
        co301->sdoObjectRequest( co301->findObjectKey(CURRENT_ACTUAL_VALUE) );
        co301->sdoObjectRequest( co301->findObjectKey(STATUSWORD) );

        co301->sendNMT_lifeguard();

        motor_ptr->getActualVelocity(  &vel );
        info->set_act_velocity( (float)vel );

        motor_ptr->getActualPosition(  &pos );
        info->set_act_position( pos );

        motor_ptr->getActualCurrent(  &curr );
        info->set_act_current( (float)curr );

        info->set_motor_id( motor_id );

        info->set_status( (int) motor_ptr->getStatus() );

        ModeOperation mode =  motor_ptr->getModeOperation( );
        info->set_mode( mode );
        i++;

        if( fabs(reference_velocity[ motor_id ])  > 0.0001 &&
                info->act_velocity() == (float)0 &&
                motor_ptr->getStatus( ) == OPERATION_ENABLED )
        {
            zero_velocity_counter[ motor_id ]++;
            int max_count = 1000000 / this->getPeriod().count();
            if( zero_velocity_counter[ motor_id ] >= max_count )
            {
                motor_ptr->stopDrive( );
                printf("Apparently the motor is not able to move at the desired velocity %f\n", reference_velocity[ motor_id ]);
                printf("STOPPING motor %d for safety after %d counts \n", motor_id, max_count);
                zero_velocity_counter[ motor_id ] = 0;
                reference_velocity[ motor_id ] = 0;
            }
        }
        else { zero_velocity_counter[ motor_id ] = 0; }

        if( velocity_watchdog[ motor_id ] > -1)
        {
            int max_count = _TCP_velocity_timeout*1000 / this->getPeriod().count();
            velocity_watchdog[ motor_id ]++;

           // printf(" %d watchdog %d\n", motor_id, velocity_watchdog[ motor_id ]);

            if( velocity_watchdog[ motor_id ] >= max_count)
            {
                motor_ptr->setProfiledVelocityTarget( 0 );
                printf("Timeout. I did not receive a new velocity command within this time: %d (TCP_velocity_timeout_ms)\n", _TCP_velocity_timeout);
                printf("STOPPING motor %d for safety after %d counts \n", motor_id, max_count);
                zero_velocity_counter[ motor_id ] = 0;
                reference_velocity[ motor_id ]    = 0;
                velocity_watchdog[ motor_id ]     = -1;


            }
        }
    }
    std::string buffer = status.SerializeAsString();
    char header = 'i';
    zmq_send (publisher_port, &header , 1 , ZMQ_SNDMORE);
    int rc = zmq_send (publisher_port, buffer.c_str() , buffer.size() , 0);
    if( rc >= 0)
    {
        cycle++;
        if (cycle % 200 == 0){
            std::cout<< COLOR_WHITE;
            printf("loop %ld (size %d) \n", cycle, rc);
        }
    }
    else{
        printf("Can't send ZMQ msg (%d): %s\n", errno, zmq_strerror (errno) ); //EAGAIN
    }

    // motor_ptr->sendSync();
}


void StatusUpdater::faultDetected(UInt16 device_id, EventData const& e)
{
    CMI::MsgEvent msg;

    msg.set_timestamp( time_since_epoch<Milliseconds>() );
    msg.set_device_id( device_id );
    msg.set_event_id( CMI::MsgEvent_MsgEventID_SERVO_FAULT );
    msg.set_data( (float) CMI::any_cast<UInt32>(e.info)  );

    std::string buffer = msg.SerializeAsString();

    char header = 'e';
    zmq_send (publisher_port, &header , 1 , ZMQ_SNDMORE);
    int rc = zmq_send (publisher_port, buffer.c_str() , buffer.size() , 0);
    if( rc <0)
    {
        printf("Can't send ZMQ msg (%d): %s\n", errno, zmq_strerror (errno) ); //EAGAIN
    }
}

void StatusUpdater::targetReached(UInt16 device_id, EventData const& e)
{
    CMI::MsgEvent msg;

    msg.set_timestamp( time_since_epoch<Milliseconds>() );
    msg.set_device_id( device_id );
    msg.set_event_id( CMI::MsgEvent_MsgEventID_POSITION_REACHED );
    msg.set_data( 0 );

    std::string buffer = msg.SerializeAsString();

    char header = 'e';
    zmq_send (publisher_port, &header , 1 , ZMQ_SNDMORE);
    int rc = zmq_send (publisher_port, buffer.c_str() , buffer.size() , 0);
    if( rc <0)
    {
        printf("Can't send ZMQ msg (%d): %s\n", errno, zmq_strerror (errno) ); //EAGAIN
    }
}

/*
void StatusUpdater::timeout(int device_id)
{
    CMI::MsgEvent msg;

    msg.set_timestamp( time_since_epoch<Milliseconds>() );
    msg.set_device_id( device_id );
    msg.set_event_id( CMI::MsgEvent
                      );
    msg.set_data( (float) boost::any_cast<UInt32> ( e.data ) );

    char header = 'e';
    zmq_send (publisher_port, &header , 1 , ZMQ_SNDMORE);
    int rc = zmq_send (publisher_port, buffer.c_str() , buffer.size() , 0);
    if( rc <0)
    {
        printf("Can't send ZMQ msg (%d): %s\n", errno, zmq_strerror (errno) ); //EAGAIN
    }
}
*/
