
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
   message(STATUS "Setting build type to 'Release' as none was specified.")
   set(CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build." FORCE)
   # Set the possible values of build type for cmake-gui
   set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "RelWithDebInfo")
 endif()
 
 message(STATUS "Build type: ${CMAKE_BUILD_TYPE}")
 
 ############################################################################
if( CERBERUS )

    SET( LIBDIR_SUFFIX "-arm" )
    MESSAGE( "Arm platform: 32 bits" )
    SET( BITS "-32" )

else()

    if( CMAKE_SIZEOF_VOID_P EQUAL 8 OR FORCE_64BIT)
        MESSAGE(STATUS "64 bits compiler detected" )
        SET( LIBDIR_SUFFIX "-x64" )
        SET( BITS "-64" )
        SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m64")

    elseif( CMAKE_SIZEOF_VOID_P EQUAL 4 OR FORCE_32BIT)

        MESSAGE(STATUS "32 bits compiler detected" )
        SET( LIBDIR_SUFFIX "-x86" )
        SET( BITS "-32" )
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m32")

    endif( )

endif ()

############################################################################

if(WIN32)

    SET(PLATFORM "win")
    message(STATUS "Compiling for Windows")

    if (MINGW )
        SET( COMPILER "-mingw-48" )
    elseif( MSVC )
        SET( COMPILER "-msvc11" )
    endif()

    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DBOOST_ALL_DYN_LINK")
############################################################################

elseif(UNIX)

    SET(PLATFORM "linux")
    message(STATUS "Compiling for Linux")

    if(NOT  CMAKE_COMPILER_IS_GNUCXX )
        message(FATAL_ERROR "Need to be compiled with GCC (version 4.8 currently supported)")
    endif()

    execute_process(COMMAND ${CMAKE_C_COMPILER} -dumpversion  OUTPUT_VARIABLE GCC_VERSION)
    #remove the new line
    string(REPLACE "\n" "" GCC_VERSION ${GCC_VERSION})
    #remove the new dot
    string(REPLACE "." "" GCC_VERSION_STRIPPED ${GCC_VERSION})

    SET( COMPILER "-gcc${GCC_VERSION_STRIPPED}" )

    if( NOT CERBERUS)
    
        IF (GCC_VERSION_STRIPPED VERSION_EQUAL 48 OR GCC_VERSION_STRIPPED VERSION_EQUAL 46)
            message(STATUS "GCC Version ${GCC_VERSION}: Ok")
        else()
            message(FATAL_ERROR "GCC Version 4.6 or 4.8 required")
        endif()
        set( LIB_SUFFIX ${COMPILER}${BITS} )
        set( TA_SUFFIX ${BITS} )
    else()
        set( LIB_SUFFIX -A5 )
        set( TA_SUFFIX "-arm" )
    endif()

    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DBOOST_ALL_DYN_LINK -DLINUX")

else()
        message( FATAL_ERROR "Platform not supported")
endif()
############################################################################

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DCMI_LIB_SUFFIX=${LIB_SUFFIX}")

set( CMI_DEPENDENCIES
     cmi${LIB_SUFFIX}
     cmi_os${LIB_SUFFIX}
      CMI_system${LIB_SUFFIX}
      CMI_chrono${LIB_SUFFIX}
      CMI_thread${LIB_SUFFIX}
      CMI_log${LIB_SUFFIX}
      CMI_log_setup${LIB_SUFFIX}
      TurboActivate${TA_SUFFIX}
    )

if(WIN32)

    set (CMI_DEPENDENCIES ${CMI_DEPENDENCIES}
            pthread
            ws2_32
    )

elseif(UNIX)

    set (CMI_DEPENDENCIES ${CMI_DEPENDENCIES}
            pthread
            dl
            rt
    )
else()
        message( FATAL_ERROR "Platform not supported")
endif()

############################################################################
