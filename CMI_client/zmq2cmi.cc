#include "msg_zmq_cmi.pb.h"
#include "zmq2cmi.h"
#include <zmq.h>
#include <set>
#include <exception>

#ifdef LINUX
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t _zmq_client_mutex = PTHREAD_MUTEX_INITIALIZER;
void mutex_lock()   { pthread_mutex_lock( &_zmq_client_mutex ); }
void mutex_unlock() { pthread_mutex_unlock( &_zmq_client_mutex ); }
#else
#include <Windows.h>
CRITICAL_SECTION _zmq_client_mutex;

#ifndef WINDOWS
#define WINDOWS
#endif

void mutex_lock()   { 
    EnterCriticalSection( &_zmq_client_mutex );
}
void mutex_unlock() { 
    LeaveCriticalSection( &_zmq_client_mutex );
}

#endif


CMI::MsgCommand _current_command;
CMI::MsgStatus  _last_status;

bool _connection_tested = false;

void* _context         = NULL; //zmq_ctx_new ();
void* _client_sender   = NULL; //zmq_socket (_context, ZMQ_REQ);
void* _client_receiver = NULL;//zmq_socket (_context, ZMQ_SUB);
bool _RECEIVER_LOOP_ = true;

#ifdef LINUX
void* receiver_thread_func(void*);
#else
DWORD WINAPI receiver_thread_func( LPVOID );
#endif

std::set<unsigned> _available_motor_ids;

void(*_cmi_target_reached_callback)(int) = NULL;

void cmiSetTargetReachedCallback( void(*callback)(int) )
{
    _cmi_target_reached_callback = callback;
}

void(*_cmi_motor_fault_callback)(int,long unsigned) = NULL;

void cmiSetMotorFaultCallback( void(*callback)(int, long unsigned) )
{
    _cmi_motor_fault_callback = callback;
}

bool cmiConnectClient(const char* server_ip, int update_frequency, long int timeout_ms)
{
#ifdef WINDOWS
    InitializeCriticalSection( &_zmq_client_mutex );
#endif

    if( _context == NULL)			_context = zmq_ctx_new ();
    if( _client_sender == NULL)		_client_sender = zmq_socket (_context, ZMQ_REQ);
    if( _client_receiver == NULL)   _client_receiver = zmq_socket (_context, ZMQ_SUB);

    char temp[100];
    sprintf(temp, "tcp://%s:5999", server_ip);

    printf("Connecting to Port REP of the Server: %s\n", temp);

    if( zmq_connect(_client_sender,   temp ) != 0)
    {
        printf("can't connect sender to %s:5999\n", server_ip);
        throw CMI_Exception (   "Cant connect to port 5999 of remote server");
    }

    sprintf(temp, "tcp://%s:5998", server_ip);

    if( zmq_connect(_client_receiver, temp ) != 0)
    {
        printf("can't connect receiver to %s:5998\n", server_ip);
        throw CMI_Exception("Cant connect to port 5998 of remote server");
    }

    zmq_setsockopt(_client_receiver,ZMQ_SUBSCRIBE, NULL ,0);

    // start the thread to keep status up to date.

#ifdef LINUX
    pthread_t thread1;
    int ret = pthread_create( &thread1, NULL, receiver_thread_func, NULL) ;
#else
    CreateThread( NULL, 0, receiver_thread_func, NULL, 0, NULL);
#endif

    long int time_counter = 0;
    while(!_connection_tested )
    {
        if( timeout_ms >0 && time_counter > timeout_ms)
        {
            printf("Timeout: can't get any answer from the server\n");
            throw CMI_Exception("Timeout");
        }
        ms_sleep(10);
        time_counter += 10;
    }

    cmiClearMsg();
    _current_command.set_master_cmd(CMI::MsgCommand_MasterCommandType_UPDATE_FREQUENCY);
    _current_command.set_update_frequency( update_frequency );
    cmiSendMsg();

    return true;
}

void cmiClearMsg( )
{
    _current_command.clear_axis_cmd();
    _current_command.clear_master_cmd();
}

#ifdef ERROR_NOT_CONNECTED
#undef ERROR_NOT_CONNECTED
#endif

CommandResult cmiSendMsg()
{
    if( _connection_tested == false )
    {
        cmiClearMsg();
        return ERROR_NOT_CONNECTED;
    }

    bool error = false;
    for(int i=0; i< _current_command.axis_cmd_size(); i++)
    {
        int motor_id = _current_command.axis_cmd(i).motor_id();
        if( cmiHasMotor( motor_id ) == false)
        {
            printf("ERROR: Master doesn't have motor ID: %d\n", motor_id);
            error = true;
        }
    }
    if(error) return ERROR_NOT_AVAILABLE;

    std::string buffer = _current_command.SerializeAsString();
    char reply[20];


    zmq_send (_client_sender, buffer.c_str() , buffer.size() , 0);
    zmq_recv (_client_sender, reply, 10, 0);

    cmiClearMsg();
    return SUCCESSFUL;
}


void cmiAppendSetPosition(unsigned motor_id, Radians target_position, unsigned int flags)
{
    CMI::MsgCommand_AxisCommand* cmd = _current_command.add_axis_cmd();

    cmd->set_motor_id( motor_id );
    cmd->set_type( CMI::MsgCommand_AxisCommandType_SET_POSITION);
    cmd->set_argument_1( target_position );
    cmd->set_argument_2( static_cast<float>(flags) );
}

void cmiAppendSetVelocity(unsigned motor_id, RadPerSec reference_velocity)
{
    CMI::MsgCommand_AxisCommand* cmd = _current_command.add_axis_cmd();

    cmd->set_motor_id( motor_id );
    cmd->set_type( CMI::MsgCommand_AxisCommandType_SET_VELOCITY);
    cmd->set_argument_1( reference_velocity );
}

void cmiAppendSetProfileParameter( unsigned motor_id, ProfileParameter param, float value)
{
    CMI::MsgCommand_AxisCommand* cmd = _current_command.add_axis_cmd();

    cmd->set_motor_id( motor_id );
    cmd->set_type( CMI::MsgCommand_AxisCommandType_SET_PROFILE_PARAMETER);
    cmd->set_argument_1( static_cast<float>(param) );
    cmd->set_argument_2( value );
}

void cmiAppendSetCurrentLimit( unsigned motor_id, MilliAmperes max_current)
{
    CMI::MsgCommand_AxisCommand* cmd = _current_command.add_axis_cmd();

    cmd->set_motor_id( motor_id );
    cmd->set_type( CMI::MsgCommand_AxisCommandType_SET_CURRENT_LIMIT);
    cmd->set_argument_1( static_cast<float>(max_current) );
}

CommandResult cmiSendResetMotor( unsigned motor_id )
{
    CMI::MsgCommand_AxisCommand* cmd = _current_command.add_axis_cmd();

    cmd->set_motor_id( motor_id );
    cmd->set_type( CMI::MsgCommand_AxisCommandType_RESET_AXIS);
    return cmiSendMsg();
}

CommandResult cmiSendEmergecyStop( unsigned motor_id )
{
    CMI::MsgCommand_AxisCommand* cmd = _current_command.add_axis_cmd();

    cmd->set_motor_id( motor_id );
    cmd->set_type( CMI::MsgCommand_AxisCommandType_EMERGENCY_STOP);
    return cmiSendMsg();
}

bool cmiHasMotor(unsigned motor_id)
{
    return ( _available_motor_ids.find( motor_id ) != _available_motor_ids.end() );
}

//----------------------------------------------------------------------
#ifdef LINUX
void* receiver_thread_func(void*)
#else
DWORD WINAPI receiver_thread_func( LPVOID ) 
#endif
{
    printf("--------- started receiver thread ----------\n");

    const int milliseconds = 2000;
    zmq_setsockopt( _client_receiver, ZMQ_RCVTIMEO, &milliseconds, sizeof(milliseconds)) ;


    while(_RECEIVER_LOOP_)
    {
        zmq_msg_t msg;
        char message_type = 'x';
        int rc;

        //-------------------------------
        zmq_msg_init (&msg);
        rc = zmq_msg_recv( &msg, _client_receiver, 0);
        message_type = *((char*)zmq_msg_data( &msg ));
        zmq_msg_close (&msg);
        //-------------------------------
        zmq_msg_init (&msg);
        rc = zmq_msg_recv( &msg, _client_receiver, 0);

        //-------------------------------
        if( rc < 0)
        {
            if(  errno == EAGAIN)
            {
                _connection_tested = false;
                printf("timeout in connection with the Master Server\n");
            }
            else {
                printf("Error in communication: %s\n",  zmq_strerror (errno) );
            }
        }
        else
        {
            if( message_type == 'e')
            {
                CMI::MsgEvent e;
                e.ParseFromArray(zmq_msg_data(&msg), zmq_msg_size(&msg) );

                if( e.event_id() == CMI::MsgEvent_MsgEventID_POSITION_REACHED &&
                    _cmi_target_reached_callback != NULL )
                {
                    _cmi_target_reached_callback( e.device_id() );
                }

                if( e.event_id() == CMI::MsgEvent_MsgEventID_SERVO_FAULT &&
                    _cmi_motor_fault_callback != NULL )
                {
                    _cmi_motor_fault_callback( e.device_id(), e.data() );
                }
            }
            else if( message_type == 'i')
            {
                mutex_lock(); // protect the area where _last_status is modified.
                _last_status.ParseFromArray(zmq_msg_data(&msg), zmq_msg_size(&msg) );

                if(_connection_tested == false)
                {
                    _available_motor_ids.clear();
                    for (int i=0; i< _last_status.info_size(); i++)
                    {
                        _available_motor_ids.insert( _last_status.info(i).motor_id() );
                    }
                }
                _connection_tested = true;
                mutex_unlock();
            }
        }
        zmq_msg_close (&msg);
    }
    return 0;
}

MotorInfo cmiGetMotorInfo(unsigned motor_id)
{
    mutex_lock(); // protect the area where _last_status is accessed.

    MotorInfo info;
    info.status = STATUS_NOT_INITIALIZED;
    info.actual_current = 0;
    info.actual_velocity = 0;
    info.actual_position = 0;
    info.timestamp = 0;
    info.control_mode = UNDEFINED_MODE;

    for(int i=0; i<_last_status.info_size(); i++)
    {
        const CMI::MsgStatus_AxisInfo& axis_info = _last_status.info(i);

        if( motor_id == axis_info.motor_id() )
        {
            info.status          = static_cast<MotorStatus>(axis_info.status());
            info.actual_current  = (MilliAmperes)axis_info.act_current();
            info.actual_velocity = axis_info.act_velocity();
            info.actual_position = axis_info.act_position();
            info.timestamp       = _last_status.timestamp();
            info.control_mode    = static_cast<ModeOperation>(axis_info.mode());
        }
    }


    mutex_unlock();
    return info;
}

MasterStatus cmiGetMasterStatus()
{
    mutex_lock(); // protect the area where _last_status is accessed.
    MasterStatus s;
    s.timestamp  = _last_status.timestamp();
    s.state     = static_cast<MasterStates> (_last_status.master_status());

    mutex_unlock();
    return s;
}

void cmiDisconnectClient()
{
    _RECEIVER_LOOP_ = false;
    zmq_ctx_destroy( _context );

#ifdef WINDOWS
    DeleteCriticalSection ( &_zmq_client_mutex );
#endif
}
