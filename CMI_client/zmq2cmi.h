// These definitions are just a reminder of the unit to be used in the API.
#include <stdio.h>
#include <MAL_types.h>
#include <stdlib.h>
#include <exception>

#ifdef LINUX
#include <unistd.h>
#define ms_sleep(x) usleep(1000*x)
#else 
#define _WINSOCKAPI_ 
#include <Windows.h>
#define ms_sleep(x) Sleep(x)
#endif
#include <iostream>

typedef float Radians;
typedef float RadPerSec;
typedef float RadPerSec2;
typedef long long signed    MilliAmperes;
typedef long long unsigned  MilliSeconds;

using namespace CanMoveIt;

/** INTRODUCTION
 * Using the Remote Client is very simple. You only need to remember some basic concepts.
 *
 * CONNECTION
 * You connect to the server through TCP/IP. Therefore you need to know the IP of the Master in you network.
 *  If you can't connect, please check that the Master can be detected usign the command "ping"
 *
 * SENDING MESSAGES
 * To reduce latencies and make the protocol more efficient, multiple commands can be packed in a single message.
 * You will note that most of the commands in the API have the prefix "Append". This mean that the command is appended
 *  to a list but it is actually delivered only when cmiSendMsg() is invoked.
 *
 * MOTOR IDENTIFIERS
 * Each motor on the Master side has been configure with a unique identifier (that is not related to the actual node ID
 * of the CANopen protocol layer.
 * The motor ID is like a telephone number to be used to call a person or an email address to send an email to someone.
 * Clients are supposed to know the motor IDs of the servo drive they want to control.
 * You can double check if a Motor ID is present using the command cmiHasMotors.
 *
 * STATUS
 * The client receive periodically the status of motors and the master itself periodically.
 * Such information can be accessed using the functions cmiGetMasterStatus() and cmiGetMotorInfo().
 * This information is not buffered, therefore you will be able to access only the most up-to-date information.
 *
 * */

/** Connect to the MASTER server using the network IP of the master (TCP protocol is used).
 *
 *  Example: cmiConnectClient("192.169.1.99");
 *
 * You can optionally specify an update frequency that tells to the master how many times
 * per second the status of the drives should be sent to the client. By default this information is updated
 * at 10 Hz. The maximum is 100 Hz.
 *
 * If a timeout is specified (>0) and the client can not connect to the server, an exception will be thrown.
 */
bool cmiConnectClient(const char* server_ip, int update_frequency = 20, long int timeout_ms = 0);

/** Check if a server is managing a certain motor ID.
 **/
bool cmiHasMotor(unsigned motor_id);

/// Disconnect the client.
void cmiDisconnectClient();

/** Clear the message (remoe all the appended commands).
 *  Note that this is done automatically by cmiSendMsg so most of the time you don't need to use this function.
 */
void cmiClearMsg();

/** The commands are sent to the server in a batched mode. In other words, multiple commands can be appended to a
 * single message.
 *
 * - cmiSetPosition
 * - cmiPosMaxAccVel
 * - cmiSetVelocity
 * - cmiSetCurrentLimit
 * - cmiResetMotorFault
 *
 * Once all the motor commands has been added, call cmiSendMsg() that will actually send the message to the server.
 */
CommandResult cmiSendMsg();

/** Set the target position. The maximum acceleration and velocity of the trapezoidal trajectory can be modified
 * with the command cmiSetMaxAccVel.
 * If necessary, this command also change the control mode to profiled position.
 * You can change the behaviour of this command using the flags specified in the type ProfiledPositionFlags.
 */
void cmiAppendSetPosition(unsigned motor_id, Radians target_position, unsigned int flags = 0);

/** Set the target velocity. The maximum acceleration and velocity can be modified
 * with the command cmiSetMaxAccVel.
 * If necessary, this command also change the control mode to profiled velocity.
 */
void cmiAppendSetVelocity(unsigned motor_id, RadPerSec reference_velocity);

/** Set the limits of the acceleration and velocity of both the position and velocity control modes.
 */
void cmiAppendSetProfileParameter( unsigned motor_id, ProfileParameter param, float value);

/** Set the limits of the current. This command will limit the effort of the motor; please check that
 * the continuos rated current of the motor is not exceeded.
 */
void cmiAppendSetCurrentLimit( unsigned motor_id, MilliAmperes max_current);

/** Stop the motor as fast as possible.
 *
 * return 0 if succesfull, error code in case of failure.
 */
CommandResult cmiSendEmergecyStop(unsigned motor_id);

/** This command is necessary only if:
 *  - A fault has been detected and you want to reset such fault.
 *  - The motor was stopped with the command cmiEmergencyStop.
 *
 * return 0 if succesfull, error code in case of failure.
 */
CommandResult cmiSendResetMotor( unsigned motor_id );

typedef struct
{
    MilliSeconds     timestamp;
    Radians          actual_position;
    RadPerSec        actual_velocity;
    MilliAmperes     actual_current;
    MotorStatus      status;
    ModeOperation    control_mode;
}MotorInfo;

typedef enum{
    MASTER_OK = 0,
    MASTER_FAULT = 1
}MasterStates;

typedef struct{
    MilliSeconds    timestamp;
    MasterStates    state;
}MasterStatus;

/** Assign a callback that will be executed every time a position reached is detected.
 * The signature of the callback must be something like:
 *
 *  void TargetReached(int motor_id)
 */
void cmiSetTargetReachedCallback( void(*callback)(int));

/** Assign a callback that will be executed every time a position reached is detected.
 * The signature of the callback must be something like:
 *
 *  void TargetReached(int motor_id)
 */
void cmiSetMotorFaultCallback( void(*callback)(int, long unsigned) );

enum{
    TIMEOUT_VELOCITY_WATCHDOG = 0x01FFFFFF
};


/** Get the most up-to-date information about a specific motor.
*   You can see if the information is new of obsolate looking at the timestamp.
*   This data structure is updated at the same frequency specified in cmiConnectClient.
*/
MotorInfo cmiGetMotorInfo(unsigned motor_id);

/** Similar to cmiGetMotorInfo, but it gets information about the status of the master itself. */
MasterStatus cmiGetMasterStatus();

class CMI_Exception : public std::exception
{
    std::string _msg;
    const char * what () const throw () { return _msg.c_str(); }
public:
    CMI_Exception(const char* msg): _msg(msg) {}
    ~CMI_Exception() throw() {}
};



