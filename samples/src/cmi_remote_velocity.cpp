#include <zmq2cmi.h>

// Author:        Davide Faconti
// Email:         facontidavide@icarustechnology.com
// Revision date: 2013/02/27

/*
In this example we will see:

-) How to connect to the server.
-) How to set the velocity of a motor.
-) How to stop a motor (in case of emergency) and restart it.
-) We will see the safety whatchdog in action.

*/

int main(int argc, char** argv)
{
	try{
		std::string server_ip( "192.168.1.99");
		if( argc == 1 || argc >3)
		{
			printf("The motor number is missing. Refer to the file gateway_server.xml to see the available motor_id numbers.\n\n");
			printf("Usage: %s motor_id (serverip)\n", argv[0]);
			printf("Example: %s 0 192.168.1.99\n", argv[0]) ;
			return 1;
		}

		if( argc == 3)
		{
			server_ip = std::string(argv[2]);
		} 

		// We are now connected to the server.
		// In this example we will send different velocities to a single motor.

		int motor_id = atoi(argv[1]);
		cmiConnectClient( server_ip.c_str(),20, 3000 );

		// Let's reset the motor just in case. In this way we are sure that it will
		// be intialized correctly.
		cmiSendResetMotor(motor_id);

		for (int i=0; i< 5; i++)
		{
			printf("\nSet velocity %f\n", 10.0*(1+i));

			// I just append the command to a message.
			cmiAppendSetVelocity(motor_id, RadPerSec(10*(1+i)) );

			// Eventually I send the message
			cmiSendMsg();

			ms_sleep(1000);
			// Let's read the current status of the motor
			MotorInfo info = cmiGetMotorInfo(motor_id);

			std::cout<< "velocity: "   << info.actual_velocity;
			std::cout<< "\tposition: " << info.actual_position;
			std::cout<< "\tcurrent: "  << info.actual_current;
			std::cout<< "\tstate: "    << info.status << " (" << StatusToString(info.status) << ")\n";
		}

		printf("STOP!!\n\n");
		cmiSendEmergecyStop(motor_id);
		ms_sleep(1000);

		printf("Restart\n\n");
		cmiSendResetMotor(motor_id);

		printf("Velocity to 100\n\n");
		cmiAppendSetVelocity(motor_id, 100);
		cmiSendMsg();
		ms_sleep(2000);
	}
	catch(std::exception &e)
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}

	// Note that after few seconds the motor will stop automatically.
	// This is caused by the fact that a watchdog will stops any motor in
	// velocity mode after 5 seconds if we don't receive any velocity command.

	// Motivation: If the TCP connection between the client and the server is lost and there is no way to re-establish it
	// (network is down, ethernet cable is damaged, client computer doesn't respond, client application crash, etc.),
	// there will be NO way to tell the motors to stop. We generally consider safer to stop the motors in such a case.

	// Your client should send a velocity command at least once every 5 seconds.

	return 1;
}
