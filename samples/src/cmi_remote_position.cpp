#include <zmq2cmi.h>
#include <iostream>
#include <iomanip>

// Author:        Davide Faconti
// Email:         facontidavide@icarustechnology.com
// Revision date: 2013/02/19

/*
  This example shows the current API related to the PROFILE_POSITION_MODE.
  In particular it shows:

  -) How to set the acceleration, velocity and deceleration of the trapezoidal interpolator.
  -) How to set an aboslute or relative target postion.
  -) How to send at once multiple points using the flag PROFILE_BUFFERED_POINT.
  -) How to create a calbback that is executed when the profiled position is reached.
 */

int PHASE = 0;


void TargetReached(int motor_id)
{
    printf("--- target reached (motor %d) -----\n", motor_id);
    PHASE++;
}

int main(int argc, char** argv)
{
    std::string server_ip( "192.168.1.99" );
    if( argc == 1 || argc >3)
    {
        printf("The motor number is missing. Refer to the file gateway_server.xml to see the available motor_id numbers.\n\n");
        printf("Usage: %s motor_id (serverip)\n", argv[0]);
        printf("Example: %s 0 192.168.1.99\n", argv[0]) ;
        return 1;
    }
    if( argc == 3)
    {
        server_ip = std::string(argv[2]);
    }

    cmiConnectClient( server_ip.c_str() );
    int motor_id = atoi(argv[1]);

    // Assign the callback
    cmiSetTargetReachedCallback( TargetReached );

    // The following parameters will be used to generate the trapezoidal profile.
    // Note as we append multiple commands and we send only one message with cmiSendMsg.
    cmiAppendSetProfileParameter(motor_id, PROFILE_VELOCITY, 100);
    cmiAppendSetProfileParameter(motor_id, PROFILE_ACCELERATION, 500);
    cmiAppendSetProfileParameter(motor_id, PROFILE_DECELERATION, 100);

    // Just print on screen the actual position and velocity.
    MotorInfo info = cmiGetMotorInfo( motor_id );
    std::cout << info.actual_position << " / " << info.actual_velocity << "\n";

    std::cout << std::setprecision(1) << std::fixed ;
    std::cout << "----- Set target position 250  ----------------------- \n";
    cmiAppendSetPosition(motor_id, 250 );
    cmiSendMsg();

    bool CONTINUE = true;
    while( CONTINUE )
    {
        ms_sleep(100);
        // Just print on screen the actual position and velocity.
        MotorInfo info = cmiGetMotorInfo( motor_id );

        if ( PHASE == 1)
        {
            PHASE = 2;
            std::cout << "----- Set target position -100 (Relative)  ------------ \n";
            // We expect the motor to move from the position 250 to 150.
            cmiAppendSetPosition(motor_id, -100, PROFILE_RELATIVE_POS );
            cmiSendMsg();
        }
        else if ( PHASE == 3)
        {
            PHASE = 4;
            std::cout << "----- Set two buffered absolute positions: 75 and 0 ---- \n";
            cmiAppendSetPosition(motor_id, 75, PROFILE_BUFFERED_POINT );
            cmiAppendSetPosition(motor_id, 0, PROFILE_BUFFERED_POINT );
            cmiSendMsg();
        }
        else if( PHASE>=6)
        {
            CONTINUE = false;
        }
        std::cout << "PHASE: " << PHASE << " position and velocity: " << info.actual_position << " / " << info.actual_velocity << "\n";
    }
    return 1;
}
