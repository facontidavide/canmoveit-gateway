/*CanMoveIt Remote client 1.0.1
Copyright (c) 2014, Icarus Technology SL, All rights reserved.

Contact: Davide Faconti  faconti@icarustechnology.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "tclap/CmdLine.h"
#include <zmq2cmi.h>


// Author:        Davide Faconti
// Email:         facontidavide@icarustechnology.com
// Revision date: 2013/02/27

/*
  This command line utility use the library boost::program_options.
  It can be used to:

  -) Send position of velocity commands.
  -) Reset a motor in fault.
  -) Display the current status of the motor.
 */

int main(int argc, char** argv)
{
    try {

        TCLAP::CmdLine cmd("Command Line interface to send command to the CanMoveIt TCP gateway", ' ', "1.0");

        // Define a value argument and add it to the command line.
        // A value arg defines a flag and a type of value that it expects,
        // such as "-s 192.168.1.99".
        TCLAP::ValueArg<std::string> serverIP("s","serverip","IP address of the server",false,"192.168.1.99","string");


        TCLAP::ValueArg<int>    motor(   "m","motor",   "Mandatory: axis of the motor",       false , 0, "int");
        TCLAP::ValueArg<double> velocity("v","velocity","Velocity command to send in rad/sec",false, 0, "double");
        TCLAP::ValueArg<double> position("p","position","Position command to send in rad",    false, 0, "double");

        // A switch arg is a boolean argument and only defines a flag that
        // indicates true or false.
        TCLAP::SwitchArg  reset( "r","reset",  "Reset the motor (to be used after a fault");
        TCLAP::SwitchArg  getinfo(  "i","info",   "Display some info about the motor");

        cmd.add(serverIP);
        cmd.add(motor);
        cmd.add(velocity);
        cmd.add(position);
        cmd.add(reset);
        cmd.add(getinfo);

        cmd.parse( argc, argv );

		 if ( !motor.isSet() )
		 {
			 std::cout << "The command -m / --motor is mandatory. Stopping the execution.\n";
             throw CMI_Exception("Motor number not specified");
		 }

        int motor_id = motor.getValue();

        //----------------------------------------------
        cmiConnectClient( serverIP.getValue().c_str() );

        MotorInfo info = cmiGetMotorInfo( motor_id );

        if ( reset.getValue() )
        {
            cmiSendResetMotor(motor_id);
            ms_sleep(100);
            info = cmiGetMotorInfo( motor_id );
        }

        if ( getinfo.getValue() )
        {
            std::cout << "----------------------------\n";
            std::cout << "timestamp:       " << info.timestamp << "\n";
            std::cout << "actual_position: " << info.actual_position << "\n";
            std::cout << "actual_velocity: " << info.actual_velocity << "\n";
            std::cout << "actual_current:  " << info.actual_current << "\n";
            std::cout << "status:          " << StatusToString(info.status) << "\n";
            std::cout << "control_mode:    " << ControlModeToString(info.control_mode) << "\n";
            std::cout << "----------------------------\n";
        }

        if ( velocity.isSet() )
        {
            printf("\nSet velocity [%d]: %f\n",motor_id, velocity.getValue() );
            cmiAppendSetVelocity(motor_id, velocity.getValue());
            cmiSendMsg();

            if( info.status != OPERATION_ENABLED)
            {
                std::cout << "The motor status is "<< StatusToString(info.status) << "\n";
                std::cout << "This means that the command \"velocity\" was probably ignored.\n";
                if( info.status != STATUS_NOT_INITIALIZED)
                    std::cout << "Please use the command --reset / -r to reset the board\n";
            }
        }

        if ( position.isSet() )
        {
            printf("\nSet position [%d]: %f\n",motor_id, position.getValue());
            cmiAppendSetPosition(motor_id, position.getValue());
            cmiSendMsg();

            if( info.status != OPERATION_ENABLED)
            {
                std::cout << "The motor status is "<< StatusToString(info.status) << "\n";
                std::cout << "This means that the command \"velocity\" was probably ignored.\n";
                if( info.status != STATUS_NOT_INITIALIZED)
                    std::cout << "Please use the command --reset / -r to reset the board\n";
            }
        }
    }
    catch(std::exception& e) {
        std::cout << "Exception: " << e.what() << "\n";
		std::cout << "Press enter to continue. Use argument --help to get more information about usage.\n";
		getchar();
        return 1;
    }
    catch(...) {
        std::cout << "Exception\n";
		std::cout << "Press enter to continue. Use argument --help to get more information about usage.\n";
		getchar();
		return 1;
    }
	std::cout << "Press enter to continue.\n";
	getchar();
    return 0;
}
