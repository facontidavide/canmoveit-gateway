#include <zmq2cmi.h>
#include <iostream>

// Author:        Davide Faconti
// Email:         facontidavide@icarustechnology.com
// Revision date: 2013/02/19

/*
  Apparently this example isn't very interesting... It just start the motor with
  a constant velocity of 20 rad/sec.
  The real purpose of this test is to show how we can reset a fault once it is detected.
  You can inject a fault either unconnecting the encoder or causing an undervoltage.

  We will also use a callback to detect and parse the fault.
 */
#include "Ingenia.h"
void FaultCallback(int motor_id, long unsigned error_code)
{
    printf("Fault Received %d: %s\n", motor_id, EmergencyCodeToString(error_code) );
}

int main(int argc, char** argv)
{
    std::string server_ip( "192.168.1.99" );
    if( argc == 1 || argc >3)
    {
       printf("The motor number is missing. Refer to the file gateway_server.xml to see the available motor_id numbers.\n\n");
       printf("Usage: %s motor_id (serverip)\n", argv[0]);
       printf("Example: %s 0 192.168.1.99\n", argv[0]) ;
       return 1;
    }
    if( argc == 3)
    {
        server_ip = std::string(argv[2]);
    }

    cmiConnectClient( server_ip.c_str() );
    int motor_id = atoi(argv[1]);

    // Let's reset the motor just in case. In this way we are sure that it will
    // be intialized correctly.
    cmiSendResetMotor(motor_id);
    cmiSetMotorFaultCallback( FaultCallback);

    while(1)
    {
        // Send periodically the desired position to prevent a "watchdog fault" on the server side.
        cmiAppendSetVelocity(motor_id, 20);
        cmiSendMsg();

        ms_sleep(1000);
        MotorInfo info = cmiGetMotorInfo(motor_id);

        std::cout<< "velocity: " << info.actual_velocity;
        std::cout<< "\tposition: " << info.actual_position;
        std::cout<< "\tcurrent: " << info.actual_current;
        std::cout<< "\tstate: " << info.status << " (" << StatusToString(info.status) << ")\n";

        // Any fault will trigger a reset.
        if( info.status != OPERATION_ENABLED )
        {
            std::cout<< "Motor is not working properly. Press Enter to reset faults\n";
            getchar();
            printf("Reset fault\n\n");
            cmiSendResetMotor(motor_id);
        }
    }
    return 1;
}
